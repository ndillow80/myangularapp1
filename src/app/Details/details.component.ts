import { Component } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})

export class DetailsComponent {

  showDetails = false;
  detailsOpen = false;
  details = 'Secret Password = "tuna"';
  logs = [];
  logCounter = 0;

  displayDetails() {
    this.showDetails === false ? this.showDetails = true : this.showDetails = false;
    this.detailsOpen === false ? this.detailsOpen = true : this.detailsOpen = false;
    this.detailsOpen === false
      ? this.logs.push('Details closed ' + new Date() )
      : this.logs.push('Details opened ' + new Date() );
    console.log(this.logs);
    this.logCounter++;
  }

  clearLogs() {
    this.logs = [];
    this.logCounter = 0;
  }
}
